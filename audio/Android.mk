AUDIO_SRCS_EXIST := \
	$(shell test -d vendor/xuantie/proprietary/sources/audio && echo true || echo false)

$(warning audio hal srcs:$(AUDIO_SRCS_EXIST))
ifneq ($(AUDIO_SRCS_EXIST), true)
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := audio.primary.$(TARGET_BOARD_PLATFORM)
LOCAL_MODULE_CLASS := SHARED_LIBRARIES
LOCAL_MODULE_RELATIVE_PATH := hw
LOCAL_SRC_FILES := audio.primary.$(TARGET_BOARD_PLATFORM).so
LOCAL_VENDOR_MODULE := true
LOCAL_MODULE_SUFFIX := .so
include $(BUILD_PREBUILT)

endif
